package ni.edu.ucem.webapi.web.inventario;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.modelo.Filtros;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.serviceImpl.InventarioServiceImpl;
import ni.edu.ucem.webapi.serviceImpl.ReservacionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.core.ListApiResponse;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by stanx on 02-23-17.
 */
@RestController
@RequestMapping("/v1/reservaciones")
public class ReservacionResource {

    private final ReservacionServiceImpl reservacionService;
    private final InventarioServiceImpl inventarioServiceService;

    @Autowired
    public ReservacionResource(final ReservacionServiceImpl reservacionService, InventarioServiceImpl inventarioServiceService)
    {
        this.reservacionService = reservacionService;
        this.inventarioServiceService = inventarioServiceService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
    public ApiResponse obtener(@PathVariable("id") final int id,
                               @RequestParam(value = "fields", required = false, defaultValue = "*") final String fields)
    {
        final Reservacion reservacion = this.reservacionService.obtenerReservacion(id, fields);
        Reservacion res = new Reservacion();
        res.setId(reservacion.getId());
        res.setDesde(reservacion.getDesde());
        res.setHasta(reservacion.getHasta());
        res.set_cuarto(this.inventarioServiceService.obtenerCuarto(reservacion.getCuarto(), ""));
        res.set_huesped(this.reservacionService.obtenerHuesped(reservacion.getHuesped()));
        return new ApiResponse(Status.OK, res);
    }

    @RequestMapping(method = RequestMethod.GET, produces="application/json")
    public ListApiResponse<Reservacion> obtenerCuartos(
            @RequestParam(value = "huesped", required = false, defaultValue = "0") final Integer huesped,
            @RequestParam(value = "offset", required = false, defaultValue ="0") final Integer offset,
            @RequestParam(value = "limit", required = false, defaultValue="10") final Integer limit,
            @RequestParam(value = "fields", required = false, defaultValue = "") final String fields,
            @RequestParam(value = "cuarto", required = false, defaultValue="0") final Integer cuarto,
            @RequestParam(value = "sort", required = false, defaultValue = "") final String sort,
            @RequestParam(value = "sortOrder", required = false, defaultValue = "asc") final String sortOrder)
    {
        HashMap<String, String> diccionario = new HashMap<String, String>();
        if (!cuarto.toString().equals("0")){
            diccionario.put("cuarto", cuarto.toString());
        }
        if (!huesped.toString().equals("0")){
            diccionario.put("huesped", huesped.toString());
        }

        final Filtros paginacion = new Filtros.Builder()
                .seleccionarCampos(fields)
                .paginar(offset, limit)
                .filtrarCampos(diccionario)
                .ordenar(sort, sortOrder)
                .build();
        List<Reservacion> reservaciones;
        final int totalReservaciones;
        reservaciones = this.reservacionService.obtenerTodasReservaciones(paginacion);
        totalReservaciones = this.reservacionService.totalReservaciones();
        return new ListApiResponse<Reservacion>(Status.OK, reservaciones, totalReservaciones, offset, limit);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarReservacion(@Valid @RequestBody final Reservacion reservacion)
    {
        if(reservacion.getDesde().compareTo(new Date()) < 0){
            return validacion("La fecha desde, debe ser mayor o igual a la actual");
        }
        if (!(reservacion.getDesde().compareTo(reservacion.getHasta()) < 0)){
            return validacion("La fecha hasta, debe ser posterior a la fecha desde");
        }
        this.reservacionService.guardarReservacion(reservacion);
        return new ApiResponse(Status.OK, reservacion);
    }

    private ApiResponse validacion(final String mensaje){
        return new ApiResponse(Status.BAD_REQUEST, null,
                new ApiResponse.ApiError(Status.BAD_REQUEST.getValue(), mensaje));
    }
}
