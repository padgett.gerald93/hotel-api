package ni.edu.ucem.webapi.web.inventario;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import ni.edu.ucem.webapi.modelo.Filtros;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.core.ListApiResponse;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.serviceImpl.InventarioServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ResponseStatus;

@RestController
@RequestMapping("/v1/inventario/cuartos")
public class CuartoResource 
{
    private final InventarioServiceImpl inventarioService;
    
    @Autowired
    public CuartoResource(final InventarioServiceImpl inventarioService)
    {
        this.inventarioService = inventarioService;
    }
    
    @RequestMapping(method = RequestMethod.GET, produces="application/json")
    public ListApiResponse<Cuarto> obtenerCuartos(
            @RequestParam(value = "categoria", required = false, defaultValue = "0") final Integer categoria,
            @RequestParam(value = "offset", required = false, defaultValue ="0") final Integer offset,
            @RequestParam(value = "limit", required = false, defaultValue="10") final Integer limit,
            @RequestParam(value = "fields", required = false, defaultValue = "") final String fields,
            @RequestParam(value = "descripcion", required = false, defaultValue="") final String descripcion,
            @RequestParam(value = "sort", required = false, defaultValue = "") final String sort,
            @RequestParam(value = "sortOrder", required = false, defaultValue = "asc") final String sortOrder)
    {
        HashMap<String, String> diccionario = new HashMap<String, String>();
        diccionario.put("descripcion", descripcion);
        diccionario.put("categoria", categoria.toString());

        final Filtros paginacion = new Filtros.Builder()
                .seleccionarCampos(fields)
                .paginar(offset, limit)
                .filtrarCampos(diccionario)
                .ordenar(sort, sortOrder)
                .build();
        List<Cuarto> cuartos;
        final int totalCuartos;
        cuartos = this.inventarioService.obtenerTodosCuarto(paginacion);
        totalCuartos =  this.inventarioService.totalCuartos(descripcion, categoria);
        return new ListApiResponse<Cuarto>(Status.OK, cuartos, totalCuartos, offset, limit);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
    public ApiResponse obtener(@PathVariable("id") final int id,
                               @RequestParam(value = "fields", required = false, defaultValue = "") final String fields)
    {
        final Cuarto cuarto = this.inventarioService.obtenerCuarto(id, fields);
        return new ApiResponse(Status.OK, cuarto);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarCuarto(@Valid @RequestBody final Cuarto cuarto) 
    {
        this.inventarioService.agregarCuarto(cuarto);
        return new ApiResponse(Status.OK, cuarto);
    }
    
    @RequestMapping(method = RequestMethod.POST, produces = "application/json", 
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarCuartoConFormParams(final Short numero, final String descripcion, final Integer categoria) 
    {
        final Cuarto cuarto = new Cuarto(numero, descripcion, categoria);
        this.inventarioService.agregarCuarto(cuarto);
        return new ApiResponse(Status.OK, cuarto);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT,
            produces="application/json")
    public ApiResponse guardarCuarto(@PathVariable("id") final int id, 
            @RequestBody final Cuarto cuartoActualizado) 
    {
        final Cuarto cuarto = new Cuarto(id,
                cuartoActualizado.getNumero(),
                cuartoActualizado.getDescripcion(),
                cuartoActualizado.getCategoria());
        this.inventarioService.guardarCuarto(cuarto);
        return new ApiResponse(Status.OK, cuarto);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, 
            produces="application/json")
    public ApiResponse eliminarCuarto(@PathVariable("id") final int id) 
    {
        final Cuarto cuarto = this.inventarioService.obtenerCuarto(id, "");
        this.inventarioService.eliminarCuarto(cuarto.getId());        
        return new ApiResponse(Status.OK);
    }
}
