package ni.edu.ucem.webapi.web.inventario;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.serviceImpl.DisponibilidadServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by stanx on 02-25-17.
 */
@RestController
@RequestMapping("/v1/disponibilidad/cupos")
public class DisponibilidadResource {

    private final DisponibilidadServiceImpl disponibilidadService;

    @Autowired
    public DisponibilidadResource(DisponibilidadServiceImpl disponibilidadService) {
        this.disponibilidadService = disponibilidadService;
    }

    @RequestMapping(method = RequestMethod.GET, produces="application/json")
    public ApiResponse obtenerCuartos(
            @RequestParam(value = "fecha-ingreso") final String fechaIngreso,
            @RequestParam(value = "fecha-egreso") final String fechaEgreso,
            @RequestParam(value = "offset", required = false, defaultValue ="0") final Integer offset,
            @RequestParam(value = "limit", required = false, defaultValue="10") final Integer limit,
            @RequestParam(value = "categoria", required = false, defaultValue="0") final Integer categoria) {
        System.out.println("INGRESO: " + fechaIngreso + "; EGRESO: " + fechaEgreso);
        final Cupo cupos = this.disponibilidadService.habitacionesDisponibles(fechaIngreso, fechaEgreso, offset, limit, categoria);
        return new ApiResponse(ApiResponse.Status.OK, cupos);
    }
}
