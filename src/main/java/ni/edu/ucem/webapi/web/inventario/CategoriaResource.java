/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.web.inventario;

import java.util.HashMap;
import java.util.List;

import ni.edu.ucem.webapi.modelo.Filtros;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.core.ListApiResponse;
import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.serviceImpl.InventarioServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@RestController
@RequestMapping("/v1/inventario/categorias")
public class CategoriaResource {
    private final InventarioServiceImpl inventarioService;
    
    @Autowired
    public CategoriaResource(final InventarioServiceImpl inventarioService){
        this.inventarioService = inventarioService;
    }
    
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ListApiResponse<CategoriaCuarto> getCategories(
            @RequestParam(value = "descripcion", required = false, defaultValue="") final String descripcion,
            @RequestParam(value = "nombre", required = false, defaultValue="") final String nombre,
            @RequestParam(value = "precio", required = false, defaultValue="0") final String precio,
            @RequestParam(value = "fields", required = false, defaultValue = "*") final String fields,
            @RequestParam(value = "offset", required = false, defaultValue ="0") final Integer offset,
            @RequestParam(value = "limit", required = false, defaultValue="10") final Integer limit,
            @RequestParam(value = "sort", required = false, defaultValue = "") final String sort,
            @RequestParam(value = "sortOrder", required = false, defaultValue = "asc") final String sortOrder
    ){
        HashMap<String, String> diccionario = new HashMap<String, String>();
        if (!descripcion.toString().equals("")){
            diccionario.put("descripcion", descripcion);
        }
        if (!nombre.toString().equals("")){
            diccionario.put("nombre", nombre);
        }
        diccionario.put("precio", precio);
        System.out.println("Diccionario: " + diccionario.toString());
        final Filtros paginacion = new Filtros.Builder()
                .seleccionarCampos(fields)
                .paginar(offset, limit)
                .filtrarCampos(diccionario)
                .ordenar(sort, sortOrder)
                .build();

        final List<CategoriaCuarto> categorias;
        categorias = inventarioService.obtenerTodosCategoriaCuartos(paginacion);
        final int totalCategorias =  this.inventarioService.totalCategorias();
        return new ListApiResponse<CategoriaCuarto>(Status.OK, categorias, totalCategorias, offset, limit);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ApiResponse getCategoryById(@PathVariable("id") final int id){
        final CategoriaCuarto categoria = this.inventarioService.obtenerCategoriaCuarto(id);
        return new ApiResponse(Status.OK, categoria);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse saveCategory(@RequestBody final CategoriaCuarto categoria){
        this.inventarioService.agregarCategoriaCuarto(categoria);
        return new ApiResponse(Status.OK, categoria);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
    public ApiResponse updateCategory(@RequestBody final CategoriaCuarto categoriaActualizada, 
                                      @PathVariable("id") final int id){
        final CategoriaCuarto categoria = new CategoriaCuarto(id,
                categoriaActualizada.getNombre(), 
                categoriaActualizada.getDescripcion(),
                categoriaActualizada.getPrecio());
        this.inventarioService.guardarCategoriaCuarto(categoria);
        return new ApiResponse(Status.OK, categoria);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ApiResponse deleteCategory(@PathVariable("id") final int id){
        final CategoriaCuarto categoria = this.inventarioService.obtenerCategoriaCuarto(id);
        this.inventarioService.eliminarCategoriaCuarto(categoria.getId());
        return new ApiResponse(Status.OK, null);        
    }
    
}
