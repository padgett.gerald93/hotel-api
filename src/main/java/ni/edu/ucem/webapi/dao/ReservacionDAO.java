package ni.edu.ucem.webapi.dao;

import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;

import java.util.List;

/**
 * Created by stanx on 02-23-17.
 */
public interface ReservacionDAO {

    public Reservacion obtenerPorId(final int pId, final String fields);

    public List<Reservacion> obtenerTodos(final int offset, final int limit, final String fields, final String whereClause,
                                          final String sort, final String sortOrder);

    public void guardar(final Reservacion pCuarto);

    public int contar();

    public Huesped obtenerHuespedPorId(Integer huesped);
}
