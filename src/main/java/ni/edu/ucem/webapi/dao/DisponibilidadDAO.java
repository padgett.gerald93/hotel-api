package ni.edu.ucem.webapi.dao;

import ni.edu.ucem.webapi.modelo.Cupo;

import java.util.Date;
import java.util.List;

/**
 * Created by stanx on 02-25-17.
 */
public interface DisponibilidadDAO {
    public Cupo verDisponibilidad(final String fechaIngreso, final String fechaEgreso, final int offset, final int limit,
                                  final int categoria);
}
