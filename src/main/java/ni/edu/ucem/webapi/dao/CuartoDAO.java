package ni.edu.ucem.webapi.dao;

import java.util.List;

import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Filtros;

public interface CuartoDAO 
{
    public Cuarto obtenerPorId(final int pId, final String fields);

    public List<Cuarto> obtenerTodos(final int offset, final int limit, final String fields, final String whereClause,
                                     final String sort, final String sortOrder);

    public List<Cuarto> obtenerTodosPorCategoriaId(int pCategoriaId, final int offset, final int limit, final String fields);

    public void agregar(final Cuarto pCuarto);

    public void guardar(final Cuarto pCuarto);

    public void eliminar(final int pId);

    public int contar(final String descripcion, final int categoria);
}
