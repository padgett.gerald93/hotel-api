package ni.edu.ucem.webapi.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.modelo.Filtros;
import ni.edu.ucem.webapi.modelo.Reservacion;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ni.edu.ucem.webapi.dao.CategoriaCuartoDAO;
import ni.edu.ucem.webapi.dao.CuartoDAO;
import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.service.InventarioService;

@Service
public class InventarioServiceImpl implements InventarioService 
{
    private final CategoriaCuartoDAO categoriaCuartoDAO;
    private final CuartoDAO cuartoDAO;

    public InventarioServiceImpl(final CategoriaCuartoDAO categoriaCuartoDAO,
            final CuartoDAO cuartoDAO)
    {
        this.categoriaCuartoDAO = categoriaCuartoDAO;
        this.cuartoDAO = cuartoDAO;

    }
    @Transactional
    @Override
    public void agregarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto) 
    {
        this.categoriaCuartoDAO.agregar(pCategoriaCuarto);
    }

    @Transactional
    @Override
    public void guardarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto) 
    {
        if(pCategoriaCuarto.getId() < 1)
        {
            throw new IllegalArgumentException("La categoría del cuarto no existe");
        }
        this.categoriaCuartoDAO.guardar(pCategoriaCuarto);
    }

    @Transactional
    @Override
    public void eliminarCategoriaCuarto(final int pId) 
    {
        if(pId < 1)
        {
            throw new IllegalArgumentException("ID invalido. Debe ser mayor a cero");
        }
        this.categoriaCuartoDAO.eliminar(pId);
    }

    @Override
    public CategoriaCuarto obtenerCategoriaCuarto(final int pId) 
    {
        return this.categoriaCuartoDAO.obtenerPorId(pId);
    }

    @Override
    public List<CategoriaCuarto> obtenerTodosCategoriaCuartos(final Filtros paginacion)
    {
        String whereFilter = "";
        Map<String, String> datos = paginacion.getMap();
        for (Map.Entry<String, String> entry : datos.entrySet()) {
            System.out.println(entry.getValue());
            if(entry.getValue() != "0" && entry.getKey() != "precio"){
                whereFilter += entry.getKey().toString() + " like '%" + entry.getValue() + "%' and ";
            }
        }
        if (datos.get("nombre") != null || datos.get("descripcion") != null){
            whereFilter = whereFilter.substring(0, whereFilter.length() - 4);
        }
        if(!datos.get("precio").equals("0") && (datos.get("nombre") != null || datos.get("descripcion") != null)){
            whereFilter += " and precio = " + datos.get("precio");
        } else if (!datos.get("precio").equals("0")){
            whereFilter += " precio = " + datos.get("precio");
        } else {
            whereFilter += "";
        }
        return this.categoriaCuartoDAO.obtenerTodos(paginacion.getOffset(), paginacion.getLimit(),
                paginacion.getFields(), whereFilter, paginacion.getSort(), paginacion.getSortOrder());
    }
    
    @Transactional
    @Override
    public void agregarCuarto(final Cuarto pCuarto) 
    {
        this.cuartoDAO.agregar(pCuarto);

    }
    
    @Transactional
    @Override
    public void guardarCuarto(final Cuarto pCuarto) 
    {
        if(pCuarto.getId() < 1)
        {
            throw new IllegalArgumentException("El cuarto no existe");
        }
        this.cuartoDAO.guardar(pCuarto);
    }
    
    @Transactional
    @Override
    public void eliminarCuarto(final int pId) 
    {
        if(pId < 1)
        {
            throw new IllegalArgumentException("ID invalido. Debe ser mayor a cero");
        }
        this.cuartoDAO.eliminar(pId);
    }

    @Override
    public Cuarto obtenerCuarto(final int pId, final String fields)
    {
        if (pId < 0) 
        {
            throw new IllegalArgumentException("ID inválido. debe ser mayor a cero.");
        }
        return this.cuartoDAO.obtenerPorId(pId, fields);
    }

    @Override
    public List<Cuarto> obtenerTodosCuarto(Filtros paginacion) {
        List<Cuarto> cuartos;
        String whereFilter;
        Map<String, String> datos = paginacion.getMap();
        whereFilter = (new StringBuilder())
                .append("descripcion like '%")
                .append(datos.get("descripcion"))
                .append("%'")
                .toString();
        if(!datos.get("categoria").equals("0")){
            whereFilter += " and categoria = " + datos.get("categoria");
        }
        cuartos = this.cuartoDAO.obtenerTodos(paginacion.getOffset(), paginacion.getLimit(),
                paginacion.getFields(), whereFilter, paginacion.getSort(), paginacion.getSortOrder());
        return cuartos;
    }

    @Override
    public List<Cuarto> obtenerTodosCuartoEnCategoria(final int pCategoriaCuartoId, final Filtros paginacion)
    {
        return this.cuartoDAO.obtenerTodosPorCategoriaId(pCategoriaCuartoId, paginacion.getOffset(),
                paginacion.getLimit(), paginacion.getFields());
    }

    @Override
    public int totalCuartos(final String pDescripcion, final int pCategoria){
        int categoria = pCategoria == 0 ? 0 : pCategoria;
        return this.cuartoDAO.contar(pDescripcion, categoria);
    }

    @Override
    public int totalCategorias() {
        return this.categoriaCuartoDAO.contarCategorias();
    }


}
