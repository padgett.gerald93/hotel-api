package ni.edu.ucem.webapi.serviceImpl;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.modelo.Filtros;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.service.ReservacionService;
import ni.edu.ucem.webapi.web.error.GlobalExceptionHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by stanx on 02-23-17.
 */
@Service
public class ReservacionServiceImpl implements ReservacionService {

    private final ReservacionDAO reservacionDAO;

    public ReservacionServiceImpl(final ReservacionDAO reservacionDAO) {
        this.reservacionDAO = reservacionDAO;
    }

    @Override
    public Reservacion obtenerReservacion(int pReservacion, String fields) {
        if (pReservacion < 0)
        {
            throw new IllegalArgumentException("ID inválido. debe ser mayor a cero.");
        }
        return this.reservacionDAO.obtenerPorId(pReservacion, fields);
    }

    @Override
    public List<Reservacion> obtenerTodasReservaciones(Filtros paginacion) {
        List<Reservacion> reservaciones;
        String whereFilter = "";
        Map<String, String> datos = paginacion.getMap();
        for (Map.Entry<String, String> entry : datos.entrySet()) {
            System.out.println(entry.getValue());
            if(entry.getValue() != "0"){
                whereFilter += entry.getKey().toString() + " = " + entry.getValue() + " and ";
            }
        }
        System.out.println("datos: "+ datos.toString());
        if (datos.toString().length() > 2){
            whereFilter = whereFilter.substring(0, whereFilter.length() - 4);
        }
        System.out.println(whereFilter);
        reservaciones = this.reservacionDAO.obtenerTodos(paginacion.getOffset(), paginacion.getLimit(),
                paginacion.getFields(), whereFilter, paginacion.getSort(), paginacion.getSortOrder());
        return reservaciones;
    }

    @Transactional
    @Override
    public void guardarReservacion(Reservacion pReservacion) {
        this.reservacionDAO.guardar(pReservacion);
    }

    @Override
    public int totalReservaciones() {
        return this.reservacionDAO.contar();
    }

    @Override
    public Huesped obtenerHuesped(Integer huesped) {
        return this.reservacionDAO.obtenerHuespedPorId(huesped);
    }
}
