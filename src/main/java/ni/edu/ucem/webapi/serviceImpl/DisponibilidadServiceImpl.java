package ni.edu.ucem.webapi.serviceImpl;

import ni.edu.ucem.webapi.dao.DisponibilidadDAO;
import ni.edu.ucem.webapi.modelo.Cupo;
import ni.edu.ucem.webapi.service.DisponibilidadService;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by stanx on 02-25-17.
 */
@Service
public class DisponibilidadServiceImpl implements DisponibilidadService {

    private final DisponibilidadDAO disponibilidadDAO;

    public DisponibilidadServiceImpl(DisponibilidadDAO disponibilidadDAO) {
        this.disponibilidadDAO = disponibilidadDAO;
    }

    @Override
    public Cupo habitacionesDisponibles(String fechaIngreso, String fechaEgreso, int offset, int limit, int categoria) {
        return this.disponibilidadDAO.verDisponibilidad(fechaIngreso, fechaEgreso, offset, limit, categoria);
    }
}
