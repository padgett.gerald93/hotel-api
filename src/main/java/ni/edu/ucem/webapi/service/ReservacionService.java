package ni.edu.ucem.webapi.service;

import ni.edu.ucem.webapi.modelo.Filtros;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;

import java.util.List;

/**
 * Created by stanx on 02-23-17.
 */
public interface ReservacionService {
    public Reservacion obtenerReservacion(final int pReservacion, final String fields);

    public List<Reservacion> obtenerTodasReservaciones(final Filtros paginacion);

    public void guardarReservacion(final Reservacion pReservacion);

    public int totalReservaciones();

    public Huesped obtenerHuesped(Integer huesped);
}
