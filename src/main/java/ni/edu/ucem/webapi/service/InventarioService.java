package ni.edu.ucem.webapi.service;

import java.util.List;

import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Filtros;
import ni.edu.ucem.webapi.modelo.Reservacion;

public interface InventarioService 
{
    public void agregarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto);

    public void guardarCategoriaCuarto(final CategoriaCuarto pCategoriaCuarto);

    public void eliminarCategoriaCuarto(int id);
    
    public CategoriaCuarto obtenerCategoriaCuarto(final int id);

    public List<CategoriaCuarto> obtenerTodosCategoriaCuartos(final Filtros paginacion);

    public void agregarCuarto(final Cuarto pCuarto);

    public void guardarCuarto(final Cuarto pCuarto);

    public void eliminarCuarto(final int pCuarto);

    public Cuarto obtenerCuarto(final int pCuarto, final String fields);

    public List<Cuarto> obtenerTodosCuarto(final Filtros paginacion);

    public List<Cuarto> obtenerTodosCuartoEnCategoria(final int pCategoriaCuarto, final Filtros paginacion);

    public int totalCuartos(final String pDescripcion, final int pCategoria);

    public int totalCategorias();


    
}
