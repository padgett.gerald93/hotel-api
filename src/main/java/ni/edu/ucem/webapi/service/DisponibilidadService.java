package ni.edu.ucem.webapi.service;

import ni.edu.ucem.webapi.modelo.Cupo;

import java.util.Date;

/**
 * Created by stanx on 02-25-17.
 */
public interface DisponibilidadService {
    public Cupo habitacionesDisponibles(final String fechaIngreso, final String fechaEgreso, final int offset, final int limit,
                                        final int categoria);
}
