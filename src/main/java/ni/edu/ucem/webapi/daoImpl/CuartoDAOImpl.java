package ni.edu.ucem.webapi.daoImpl;

import java.util.List;

import ni.edu.ucem.webapi.modelo.Filtros;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.integration.IntegrationAutoConfiguration;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ni.edu.ucem.webapi.dao.CuartoDAO;
import ni.edu.ucem.webapi.modelo.Cuarto;

@Repository
public class CuartoDAOImpl implements CuartoDAO 
{
    private final JdbcTemplate jdbcTemplate;
   
    @Autowired
    public CuartoDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Cuarto obtenerPorId(final int pId, final String fields)
    {
        final String fieldsFilter = fields.isEmpty() ? "*" : fields;
        String sql = "select " + fieldsFilter +" from cuarto where id = ?";

        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    }

    @Override
    public List<Cuarto> obtenerTodos(final int offset, final int limit, final String fields, final String whereClause,
                                     final String sort, final String sortOrder)
    {
        final String fieldsFilter = fields.isEmpty() ? "*" : fields;
        final String whereFilter = whereClause.isEmpty() ? "" : " where " + whereClause;
        final String sortData = sort.isEmpty() ? "order by id asc" : "order by " + sort + " " +sortOrder;
        final String sql = new StringBuilder()
                .append("select")
                .append(" ")
                .append(fieldsFilter)
                .append(" ")
                .append("from cuarto")
                .append(whereFilter)
                .append(" ")
                .append(sortData)
                .append(" ")
                .append("offset ? limit ?")
                .toString();
        return this.jdbcTemplate.query(sql, new Object[]{ offset, limit },
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    }

    @Override
    public List<Cuarto> obtenerTodosPorCategoriaId(final int categoriaId, int offset, int limit, final String fields)
    {
        final String fieldsFilter = fields.isEmpty() ? "*" : fields;
        final String sql = new StringBuilder()
                .append("select")
                .append(" ")
                .append(fieldsFilter)
                .append(" ")
                .append("from cuarto where categoria = ? offset ? limit ?")
                .toString();
        return this.jdbcTemplate.query(sql, new Object[]{categoriaId, offset, limit},
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    }

    @Override
    public void agregar(final Cuarto pCuarto) 
    {
        final String sql = new StringBuilder()
                .append("INSERT INTO cuarto")
                .append(" ")
                .append("(numero, descripcion, categoria)")
                .append(" ")
                .append("VALUES (?, ?, ?)")
                .toString();
        final Object[] parametros = new Object[3];
        parametros[0] = pCuarto.getNumero();
        parametros[1] = pCuarto.getDescripcion();
        parametros[2] = pCuarto.getCategoria();
        this.jdbcTemplate.update(sql,parametros);
        
    }

    @Override
    public void guardar(final Cuarto pCuarto) 
    {        
       final String sql = new StringBuilder()
                .append("UPDATE cuarto")
                .append(" ")
                .append("set numero = ?")
                .append(",descripcion = ?")
                .append(",categoria = ?")
                .append(" ")
                .append("where id = ?")
                .toString();
        final Object[] parametros = new Object[4];
        parametros[0] = pCuarto.getNumero();
        parametros[1] = pCuarto.getDescripcion();
        parametros[2] = pCuarto.getCategoria();
        parametros[3] = pCuarto.getId();
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void eliminar(final int pId) 
    {
        final String sql = "delete from cuarto where id = ?";
        this.jdbcTemplate.update(sql, new Object[]{pId});
    }

    @Override
    public int contar(final String descripcion, final int categoria) {
        String whereClause = "where descripcion like '%" + descripcion + "%'";
        if (categoria != 0){
            whereClause += " and categoria = " + categoria;
        }
        final String sql = new StringBuilder()
                .append("select count(*) from cuarto")
                .append(" ")
                .append(whereClause)
                .toString();
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }

}
