package ni.edu.ucem.webapi.daoImpl;

import ni.edu.ucem.webapi.dao.DisponibilidadDAO;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Cupo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by stanx on 02-25-17.
 */
@Repository
public class DisponibilidadDAOImpl implements DisponibilidadDAO {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DisponibilidadDAOImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Cupo verDisponibilidad(final String fechaIngreso, final String fechaEgreso, final int offset, final int limit,
                                  final int categoria) {
        String categoriaFiltro = "";
        if (categoria > 0) {
            categoriaFiltro = " and categoria = " + categoria;
        }
        final String desdeFiltro = "desde between '" + fechaIngreso + "' and '" + fechaEgreso + "'";
        final String hastaFiltro = "hasta between '" + fechaIngreso + "' and '" + fechaEgreso + "'";
        final String innerRangos = "desde < '" + fechaIngreso + "' and hasta > '" + fechaEgreso + "'";
        final String sql = new StringBuilder()
                .append("select * from cuarto where id not in( ")
                .append("select cuarto from reservacion where ((")
                .append(desdeFiltro)
                .append(") or (")
                .append(hastaFiltro)
                .append(") or (")
                .append(innerRangos)
                .append(")) ")
                .append(" )")
                .append(categoriaFiltro)
                .append(" ")
                .append("offset ? limit ?")
                .toString();
        final String sql1 = "SELECT cuarto.* FROM cuarto WHERE cuarto.id NOT IN ( " +
                            "SELECT cuarto FROM reservacion WHERE desde between '" + fechaIngreso + "' and '" + fechaEgreso + "') offset ? limit ?";
        System.out.println("SQL: " + sql);
        List<Cuarto> cuartos = this.jdbcTemplate.query(sql, new Object[]{offset, limit },
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
        return new Cupo(fechaIngreso, fechaEgreso, cuartos);
    }
}
