package ni.edu.ucem.webapi.daoImpl;

import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Reservacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by stanx on 02-23-17.
 */
@Repository
public class ReservacionDAOImpl implements ReservacionDAO {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ReservacionDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Reservacion obtenerPorId(final int pId, final String fields)
    {
        String sql = "select " + fields +" from reservacion where id = ? ";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId},
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
        // Agregar mas campos en reservacion y un nuevo contructor para consultar en huesped y en cuarto dependiendo
        // del jdbcTemplate
    }

    @Override
    public List<Reservacion> obtenerTodos(int offset, int limit, String fields, String whereClause, String sort,
                                          String sortOrder) {
        final String fieldsFilter = fields.isEmpty() ? "*" : fields;
        final String whereFilter = whereClause.isEmpty() ? "" : " where " + whereClause;
        final String sortData = sort.isEmpty() ? "order by id asc" : "order by " + sort + " " +sortOrder;
        final String sql = new StringBuilder()
                .append("select")
                .append(" ")
                .append(fieldsFilter)
                .append(" ")
                .append("from reservacion")
                .append(whereFilter)
                .append(" ")
                .append(sortData)
                .append(" ")
                .append("offset ? limit ?")
                .toString();
        System.out.println(sql);
        return this.jdbcTemplate.query(sql, new Object[]{ offset, limit },
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
    }

    @Override
    public void guardar(Reservacion pReservacion) {
        final String sql = new StringBuilder()
                .append("INSERT INTO reservacion")
                .append(" ")
                .append("(desde, hasta, huesped, cuarto)")
                .append(" ")
                .append("VALUES (?, ?, ?, ?)")
                .toString();
        final Object[] parametros = new Object[4];
        parametros[0] = pReservacion.getDesde();
        parametros[1] = pReservacion.getHasta();
        parametros[2] = pReservacion.getHuesped();
        parametros[3] = pReservacion.getCuarto();
        this.jdbcTemplate.update(sql, parametros);
    }

    @Override
    public int contar() {
        final String sql = "select count(*) from reservacion";
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public Huesped obtenerHuespedPorId(Integer huesped) {
        String sql = "select * from huesped where id = ? ";
        return jdbcTemplate.queryForObject(sql, new Object[]{huesped},
                new BeanPropertyRowMapper<Huesped>(Huesped.class));
    }
}
