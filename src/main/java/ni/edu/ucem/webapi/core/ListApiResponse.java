package ni.edu.ucem.webapi.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import ni.edu.ucem.webapi.core.ApiResponse.Status;

import java.util.List;

import ni.edu.ucem.webapi.core.ApiResponse.ApiError;
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListApiResponse<T>
{
    private final Status status;
    private final List<T> data;
    private final long count;
    private final int offset;
    private final int limit;
    private final ApiError error;
    
    
    public ListApiResponse(Status status, List<T> data) 
    {
        this(status,data,0,0,0,null);
    }

    public ListApiResponse(final Status status, final List<T> data, final long count,
            final int offset, int limit) 
    {
        this(status,data,count,offset,limit,null);
    }
    
    public ListApiResponse(final Status status, final List<T> data, final long count, 
            final int offset, final int limit,final ApiError error) 
    {
        this.status = status;
        this.data = data;
        this.count = count;
        this.offset = offset;
        this.limit = limit;
        this.error = error;
    }
    
    public Status getStatus() {
        return status;
    }

    public ApiError getError() {
        return error;
    }

    public List<T> getData() {
        return data;
    }

    public long getCount() {
        return count;
    }

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }
}
