package ni.edu.ucem.webapi.modelo;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * Created by stanx on 02-25-17.
 */
public class Cupo {
    private String fechaIngreso;
    private String fechaSalida;
    private List<Cuarto> cuartos;

    public Cupo(String fechaIngreso, String fechaSalida, List<Cuarto> cuartos) {
        this.fechaIngreso = fechaIngreso;
        this.fechaSalida = fechaSalida;
        this.cuartos = cuartos;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public List<Cuarto> getCuartos() {
        return cuartos;
    }

    public void setCuartos(List<Cuarto> cuartos) {
        this.cuartos = cuartos;
    }
}
