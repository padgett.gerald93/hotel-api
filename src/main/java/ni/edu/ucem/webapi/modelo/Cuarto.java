package ni.edu.ucem.webapi.modelo;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeName;
import ni.edu.ucem.webapi.dao.CuartoDAO;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import java.util.Date;
import java.util.Set;


@JsonRootName("cuartos")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Cuarto 
{
    private Integer id;
    @NotNull(message = "El número de cuarto es requerido")
    @Range(min = 1, max = Short.MAX_VALUE)
    private Short  numero;
    
    @NotBlank(message = "La descripción es requerida")
    @Pattern(regexp="^[\\w ]+$")
    private String descripcion;
    
    @NotNull(message = "La categoria es requerido")
    @Min(1)
    private Integer categoria;

    private Date fecha = new Date();
    private Set<Reservacion> books;

    public Cuarto()
    {
    }

    public Cuarto(int id){
        final CuartoDAO reservacionDAO = null;
        final Cuarto obj = reservacionDAO.obtenerPorId(id, "");
        this.descripcion = obj.getDescripcion();
        this.numero = obj.getNumero();
        this.categoria = obj.getCategoria();
    }
    
    public Cuarto(final Short numero, final String descripcion, final Integer categoria) {
        this.numero = numero;
        this.descripcion = descripcion;
        this.categoria = categoria;
    }

    public Cuarto(final Integer id, final Short numero, final String descripcion,
            final Integer categoria) 
    {
        this.id = id;
        this.numero = numero;
        this.descripcion = descripcion;
        this.categoria = categoria;
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Short getNumero() {
        return numero;
    }
    public void setNumero(final Short numero) {
        this.numero = numero;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(final String descripcion) {
        this.descripcion = descripcion;
    }
    public Integer getCategoria() {
        return categoria;
    }
    public void setCategoria(final Integer categoria) {
        this.categoria = categoria;
    }
}
