package ni.edu.ucem.webapi.modelo;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by stanx on 02-17-17.
 */
public class Filtros {
    private int offset;
    private int limit;
    private String fields;
    private HashMap<String, String> mapaFiltros;
    private String sort;
    private String sortOrder;

    public static class Builder {
        private int offset;
        private int limit;
        private String fields;
        private HashMap<String, String> mapaFiltros;
        private String sort;
        private String sortOrder;

        public Builder paginar(int offset, int limit){
            this.limit = limit;
            this.offset = offset;
            return this;
        }

        public Builder seleccionarCampos(String fields){
            this.fields = fields;
            return this;
        }

        public Builder filtrarCampos(HashMap<String, String> mapaFiltros){
            this.mapaFiltros = mapaFiltros;
            return this;
        }

        public Builder ordenar(String sort, String sortOrder) {
            this.sort = sort;
            this.sortOrder = sortOrder;
            return this;
        }

        public Filtros build(){
            Filtros filtro = new Filtros();
            if(this.limit < 1 || this.offset < 0){
                this.limit = 10;
                this.offset = 0;
            }
            filtro.setOffset(offset);
            filtro.setLimit(limit);
            filtro.setMap(mapaFiltros);
            filtro.setFields(fields);
            filtro.setSort(sort);
            filtro.setSortOrder(sortOrder);
            return filtro;
        }
    }

    public Filtros(){ }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() { return offset; }

    public void setOffset(int offset) { this.offset = offset; }

    public HashMap<String, String> getMap() { return mapaFiltros; }

    public void setMap(HashMap<String, String> mapaFiltros) { this.mapaFiltros = mapaFiltros; }

    public String getFields() { return fields; }

    public void setFields(String fields) { this.fields = fields; }

    public String getSort() { return sort; }

    public void setSort(String sort) { this.sort = sort; }

    public String getSortOrder() { return sortOrder; }

    public void setSortOrder(String sortOrder) { this.sortOrder = sortOrder; }
}
