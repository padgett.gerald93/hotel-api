package ni.edu.ucem.webapi.modelo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by stanx on 02-23-17.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Reservacion {
    private Integer id;

    @NotNull(message = "La fecha desde es requerido")
    private Date desde;

    @NotNull(message = "El cuarto es requerido")
    @Min(1)
    private Integer cuarto;

    @NotNull(message = "La fecha hasta es requerido")
    private Date hasta;

    @NotNull(message = "El huesped es requerido")
    @Min(1)
    private Integer huesped;

    private Cuarto _cuarto;

    private Huesped _huesped;

    public Reservacion() { }

    public Reservacion(final Date desde, final Date hasta, final Integer cuarto, final Integer huesped) {
        this.desde = desde;
        this.hasta = hasta;
        this.cuarto = cuarto;
        this.huesped = huesped;
    }
    public Reservacion(final Integer id, final Date desde, final Date hasta, final Integer cuarto, final Integer huesped) {
        this.id = id;
        this.desde = desde;
        this.hasta = hasta;
        this.cuarto = cuarto;
        this.huesped = huesped;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }

    public Date getHasta() {
        return hasta;
    }

    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

    public Integer getCuarto() {
        return cuarto;
    }

    public void setCuarto(Integer cuarto) {
        this.cuarto = cuarto;
    }

    public Integer getHuesped() {
        return huesped;
    }

    public void setHuesped(Integer huesped) {
        this.huesped = huesped;
    }

    public Cuarto get_cuarto() { return _cuarto; }

    public void set_cuarto(Cuarto _cuarto) {
        this._cuarto = _cuarto;
    }

    public Huesped get_huesped() {
        return _huesped;
    }

    public void set_huesped(Huesped _huesped) {
        this._huesped = _huesped;
    }
}
