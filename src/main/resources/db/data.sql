INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Individual', 'Ideal para quienes viajan solos.',50.0);
INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Familiar', 'Ideal para quienes viajan con la familia completa.',100.0);

INSERT INTO cuarto (numero, descripcion,categoria)
    VALUES(1,'Cuarto 1',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(2,'Cuarto 2',2);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(3,'Cuarto 3',2);
INSERT INTO cuarto (numero, descripcion,categoria)
    VALUES(4,'Cuarto 4',2);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(5,'Cuarto 5',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(6,'Cuarto 6',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(7,'Cuarto 7',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(8,'Cuarto 8',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(9,'Cuarto 9',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(10,'Cuarto 10',1);

INSERT INTO huesped(nombre, email, telefono)
    VALUES('Gerald Padgett', 'padgett.gerald93@gmail.com', '87388187');

INSERT INTO reservacion(desde, hasta, cuarto, huesped)
    VALUES(CURRENT_DATE(), (DATEADD('DAY', 1, CURRENT_DATE())), 1, 1);

